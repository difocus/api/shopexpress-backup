<?php

namespace ShopExpress\Backup;

use DateTime;
use Exception;
use Psr\Log\LoggerInterface;
use ShopExpress\ApiClient\ApiClient;
use ShopExpress\ApiClient\Exception\NetworkException;
use ShopExpress\ApiClient\Exception\RequestException;
use ShopExpress\ApiClient\Request\RequestBuilder;
use ShopExpress\ApiClient\Request\RequestBuilderFilter;
use ShopExpress\Backup\Entity\User;
use ShopExpress\Backup\Exception\ObjectNotFoundException;
use ShopExpress\Backup\Strategy\RateOptionFactory;
use ShopExpress\Backup\Strategy\Option\RateStubOption;
use ShopExpress\Backup\Utils\DateUtils;
use ShopExpress\RequestResponse\Exception\InvalidRequestException;
use Throwable;

class Main
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var Config
     */
    private $config;
    /**
     * @var RateOptionFactory
     */
    private $factory;
    /**
     * @var bool
     */
    private $useStubOption;

    /**
     * @var ApiClient
     */
    private $clientShopExpress;

    /**
     * @var array
     */
    private $sites;

    /**
     * @var array
     */
    private $users = [];

    /**
     * Main constructor.
     * @param LoggerInterface $logger
     * @param Config $config
     * @param RateOptionFactory $factory
     * @param bool $useStubOption
     * @param array $sites
     * @throws Exception
     */
    public function __construct(
        LoggerInterface $logger,
        Config $config,
        RateOptionFactory $factory,
        bool $useStubOption,
        array $sites = []
    )
    {
        $this->logger = $logger;
        $this->config = $config;
        $this->factory = $factory;
        $this->useStubOption = $useStubOption;
        $this->sites = $sites;
        $this->clientShopExpress = new ApiClient(
            $this->config->get('SHOPEXPRESS_URL'),
            $this->config->get('SHOPEXPRESS_KEY')
        );
    }

    /**
     * @param array $webSites
     * @throws Exception
     */
    public function run(array $webSites): void
    {
        $domainsPath = rtrim($this->config->get('PATH_DOMAINS'), '/');
        foreach ($webSites as $item) {
            if ($item !== '..' && $item !== '.' && is_dir($domainsPath . '/' . $item)) {
                try {
                    $user = $this->getInitUser($item);
                } catch (Throwable $e) {
                    $this->logger->warning("Ошибка во время инициализации User для сайта {$item}: {$e->getMessage()}");
                    continue;
                }

                $strategy = $this->factory->createByUser($user);
                $backup = new Backup($this->config, $this->logger, $strategy, new DateUtils());

                if ($strategy->checkPaid()) {
                    $backup->create($user);
                } else {
                    $this->logger->warning('Срок оплаты истек');
                }

                $backup->removeNeedlessBackups($user);

                $this->saveCreatedBackups($user);
                $this->deleteRemovedBackups($user);
            }
        }

    }

    /**
     * @param array $sites
     */
    public function setSites(array $sites): void
    {
        $this->sites = $sites;
    }

    /**
     * @param int $id
     * @return User|null
     */
    public function findUser(int $id): ?User
    {
        if (array_key_exists($id, $this->users)) {
            return $this->users[$id];
        }
        return null;
    }

    /**
     * @param User $user
     */
    private function saveCreatedBackups(User $user): void
    {
        $requestBuilder = new RequestBuilder('cabinet_backups');
        foreach ($user->getCreatedBackups() as $dateTimeBackup) {
            $requestBuilder->setData([
                'add_backup' => [
                    'date' => $dateTimeBackup,
                    'site_id' => $user->site_id,
                    'prefix' => $user->getPrefix()
                ]
            ]);
            try {
                $response = $this->clientShopExpress->create($requestBuilder);
                $this->logger->info(print_r($response->getResponse(), 1));
            } catch (Throwable $e) {
                $this->logger->warning("Не удалось создать запись о бэкапе {$dateTimeBackup}: {$e->getMessage()}");
            }
        }
    }

    /**
     * @param User $user
     */
    private function deleteRemovedBackups(User $user): void
    {
        $ids = array_keys($user->getRemovedBackups());
        if (count($ids) === 0) {
            return;
        }
        $requestBuilder = new RequestBuilder(
            'cabinet_backups',
            [new RequestBuilderFilter('id', 'in', $ids)]
        );
        try {
            $response = $this->clientShopExpress->delete($requestBuilder);
            $this->logger->info(print_r($response->getResponse(), 1));
        } catch (Throwable $e) {
            $this->logger->warning(sprintf("Не удалось удалить бэкапы с id %s: %s", implode(', ', $ids), $e->getMessage()));
        }
    }

    /**
     * @param string $siteHost
     * @return User
     * @throws ObjectNotFoundException
     * @throws Exception
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     */
    public function getInitUser(string $siteHost): User
    {
        $dataShop = $this->fetchShop($siteHost);
        $dataBackups = $this->fetchBackups($dataShop['id']);

        $user = new User();
        $user->setId($dataShop['user_oid']);
        $user->setPaidtill($dataShop['paid_till']['date']);
        $user->setData([
            'site_id' => $dataShop['id'],
            'site_host' => $dataShop['site_host'],
            'rate' => $dataShop['tariff'],
        ]);

        $backups = [];
        foreach ($dataBackups as $dataBackup) {
            $backups[$dataBackup['id']] = (new DateTime($dataBackup['created']['date']))->format(DateUtils::DATE_TIME_FORMAT);
        }

        $user->setInitialBackups($backups);
        $user->setBackups($backups);

        if (!$user->getPaidtill()) {
            $user->setPaidtill(new DateTime('tomorrow'));
        }

        if (!$user->getRates()) {
            $user->setRates('14days');
        }

        if ($this->useStubOption) {
            $user->setRates(RateStubOption::getAlias());
        }

        $this->users[$user->getId()] = $user;

        return $user;
    }

    /**
     * @param string $siteHost
     * @return array
     * @throws ObjectNotFoundException
     * @throws Exception
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     */
    private function fetchShop(string $siteHost): array
    {
        if (array_key_exists($siteHost, $this->sites)) {
            return $this->sites[$siteHost];
        }

        $requestBuilder = new RequestBuilder('cabinet_sites');
        $requestBuilder->addFilter(new RequestBuilderFilter('site_host', 'like', $siteHost));

        $response = $this->clientShopExpress->get($requestBuilder);
        $result = $response['result'];

        if ($result['success'] === false || $result['meta']['total'] === 0) {
            throw new ObjectNotFoundException("Магазин не найден");
        }

        return current($result['data']);
    }


    /**
     * @param int $siteId
     * @return array
     * @throws Exception
     * @throws InvalidRequestException
     * @throws NetworkException
     * @throws RequestException
     */
    private function fetchBackups(int $siteId): array
    {
        $requestBuilder = new RequestBuilder('cabinet_backups');
        $requestBuilder->addFilter(new RequestBuilderFilter('site_id', '=', $siteId));

        $response = $this->clientShopExpress->get($requestBuilder);

        return $response['result']['data'] ?? [];
    }
}
