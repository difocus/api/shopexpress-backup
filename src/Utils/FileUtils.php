<?php

namespace ShopExpress\Backup\Utils;


class FileUtils
{
    public const NOT_FOUND = -1;

    public static function removeBackupsExceptOne($arr, $index, $path_user_backup, callable $func)
    {
        reset($arr);
        foreach ($arr as $key => $item) {
            if ($key != $index) {
                try {
                    static::removeBackupFromStorage($item, $path_user_backup, $func);
                } catch (\Exception $e) {
                }
            }
        }
    }

    /**
     * @param $date
     * @param $path_user_backup
     * @param callable $func
     * @throws \Exception
     */
    public static function removeBackupFromStorage($date, $path_user_backup, callable $func)
    {
        $file_path = "{$path_user_backup}/db-" . $date . ".gz";
        static::removeBackupFromStorageByPath($file_path, $func);
    }

    /**
     * @param $file_path
     * @param callable $func
     * @throws \Exception
     */
    public static function removeBackupFromStorageByPath($file_path, callable $func)
    {
        clearstatcache(false, $file_path);
        if (file_exists($file_path)) {
            $retval = unlink($file_path);
            if (!$retval) {
                $func("Error! File $file_path exists, but it hasn't been deleted");
                throw new \Exception("Error! File $file_path exists, but it hasn't been deleted");
            }
            $func("File $file_path has been successfully removed from storage");

        } else {
            $func("Error, file $file_path not exists");
        }
    }

    public static function getBackupFileSize($date, $path_user_backup, callable $func)
    {
        $file_path = "{$path_user_backup}/db-" . $date . ".gz";
        clearstatcache(false, $file_path);
        if (file_exists($file_path)) {
            return filesize($file_path);
        }
        $func("File $file_path not exists");
        return self::NOT_FOUND;
    }

    /**
     * @param $file_path
     * @param $func
     * @return int|string
     */
    public static function getFileHash($file_path, $func)
    {
        clearstatcache(false, $file_path);
        if (!file_exists($file_path)) {
            $func("File $file_path not found");
            return self::NOT_FOUND;
        }
        $content = gzdecode(file_get_contents($file_path));
        return hash('md5', $content);
    }
}
