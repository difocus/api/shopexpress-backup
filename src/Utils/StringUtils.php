<?php

namespace ShopExpress\Backup\Utils;


class StringUtils
{
    public static function isSerialize(string $str): bool
    {
        return $str === 'N;' || $str === '' || preg_match("/^a:\d+:{/i", $str);
    }

    public static function isJson(string $string): bool
    {
        json_decode($string, true);
        return (json_last_error() === JSON_ERROR_NONE);
    }
}
