<?php

namespace ShopExpress\Backup\Utils;

use DateTime;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use RuntimeException;

class DateUtils
{
    public const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    /**
     * @param $dateTime string|int
     * @param string $format
     * @return string
     */
    public function format($dateTime, string $format = self::DATE_TIME_FORMAT): string
    {
        if (is_string($dateTime)) {
            $date = new DateTimeImmutable($dateTime);
        } elseif (is_int($dateTime)) {
            $date = new DateTimeImmutable("@{$dateTime}");
        } else {
            throw new RuntimeException(sprintf('Invalid type of date time value passed: %s', $dateTime));
        }

        $formatted = $date->format($format);

        if ($formatted === false) {
            throw new RuntimeException(sprintf('Can\'t convert UTC date time %s to local', $dateTime));
        }

        return $formatted;
    }

    /**
     * @param string $dateTimeStr
     * @return int
     * @throws Exception
     */
    public function getTimestamp(string $dateTimeStr): int
    {
        $date = new DateTimeImmutable($dateTimeStr);

        return $date->getTimestamp();
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getLocalDateTime(): string
    {
        $date = new DateTime();
        return $date->format(self::DATE_TIME_FORMAT);
    }

    /**
     * @return string
     */
    public function isLastDayOfMonth(): string
    {
        $currentTime = $this->getCurrentTime();
        return date('d', $currentTime) === date('t', $currentTime);
    }

    /**
     * @param int $timestamp
     * @return int
     * @throws Exception
     */
    public function getRoundTimestampToDay(int $timestamp): int
    {
        $date = new DateTime("@{$timestamp}");
        $date->setTime(0, 0, 0, 0);

        return $date->getTimestamp();
    }

    /**
     * @return int
     */
    public function getFirstDayOfMonthTimestamp(): int
    {
        return (new DateTime('first day of this month 00:00:00'))->getTimestamp();
    }

    /**
     * @param string $dateTimeStr
     * @return bool
     * @throws Exception
     */
    public function isLastWeek(string $dateTimeStr): bool
    {
        $timestamp = $this->getTimestamp($dateTimeStr);

        return $timestamp >= $this->getRoundTimestampToDay($this->getCurrentTime() - 24 * 60 * 60 * 7);
    }

    /**
     * @param string $dateTimeStr
     * @return int
     * @throws Exception
     */
    public function getDayIntervalSinceDateTime(string $dateTimeStr): int
    {
        $roundedTimestamp = $this->getRoundTimestampToDay($this->getTimestamp($dateTimeStr));
        $today = $this->getRoundTimestampToDay($this->getCurrentTime());

        return (int) round(($today - $roundedTimestamp) / 60 / 60 / 24);
    }

    /**
     * @return int
     */
    public function getCurrentTime(): int
    {
        return time();
    }

    /**
     * @param string $dateTimeStr
     * @return string|null
     */
    public function parseRdiffDateString(string $dateTimeStr): ?string
    {
        $dateTimeStr = trim($dateTimeStr, " \t");
        $dateTimeBeginPos = mb_strpos($dateTimeStr, ' ');
        if ($dateTimeBeginPos === false) {
            return null;
        }
        $dateTimeStr = mb_substr($dateTimeStr, $dateTimeBeginPos + 1);

        $dateParts = array_values(array_filter(explode(' ', $dateTimeStr), static function (string $item) {
            return trim($item) !== '';
        }));
        if (count($dateParts) !== 4) {
            return null;
        }

        return sprintf('%s-%s %s %s', $dateParts[1], $dateParts[0], $dateParts[3], $dateParts[2]);
    }

    /**
     * @param string $line
     * @return false|string
     * @throws RuntimeException
     * @throws Exception
     */
    public function parseRdiffBackupIncrementsLine(string $line)
    {
        $line = trim($line);
        if (mb_strpos($line, 'Current mirror: ') !== false) {
            $dateTimePart = str_replace('Current mirror: ', '', $line);
        } else {
            $dateTimePartPos = null;
            foreach (['   ', "\t"] as $delimiter) {
                $delimiterLastPos = mb_strrpos($line, $delimiter);
                if ($delimiterLastPos === false) {
                    continue;
                }

                $dateTimePartPos = $delimiterLastPos + mb_strlen($delimiter);
                break;
            }

            if ($dateTimePartPos === null) {
                return false;
            }

            $dateTimePart = mb_substr($line, $dateTimePartPos);
        }

        try {
            $parsedDateTimePart = $this->parseRdiffDateString($dateTimePart);
            if ($parsedDateTimePart === null) {
                return false;
            }

            $localDateTimeStr = $this->format($parsedDateTimePart);
        } catch (Exception $e) {
            throw new RuntimeException(sprintf('Increments line %s doesn\'t contains valid datetime, %s detected', $line, $dateTimePart));
        }

        return $localDateTimeStr;
    }
}
