<?php

namespace ShopExpress\Backup;

use Psr\Log\LoggerInterface;
use ShopExpress\Backup\Entity\User;
use ShopExpress\Backup\Strategy\Option\RateOptionInterface;
use Exception;
use ShopExpress\Backup\Utils\DateUtils;

class Backup
{
    private $path_backups;
    private $path_domains;

    /** @var LoggerInterface */
    private $logger;

    /**
     * @var Config
     */
    private $config;
    /**
     * @var RateOptionInterface
     */
    private $strategy;

    /**
     * @var DateUtils
     */
    public $dateUtils;

    /**
     * Backup constructor.
     * @param Config $config
     * @param LoggerInterface $logger
     * @param RateOptionInterface $strategy
     * @param DateUtils $dateUtils
     */
    public function __construct(
        Config $config,
        LoggerInterface $logger,
        RateOptionInterface $strategy,
        DateUtils $dateUtils
    )
    {
        $this->config = $config;
        $this->path_backups = $this->config->get('PATH_BACKUPS');
        $this->path_domains = $this->config->get('PATH_DOMAINS');
        $this->logger = $logger;
        $this->strategy = $strategy;
        $this->dateUtils = $dateUtils;
        $this->strategy->setBackupManager($this);
    }

    /**
     * @param User $user
     * @return array
     */
    public function removeNeedlessBackups(User $user): array
    {
        $this->strategy->setUser($user);
        $this->logger->info(sprintf('Список бэкапов в начале Backup@removeNeedlessBackups:%s%s', PHP_EOL, var_export($user->getBackups(), true)));

        $this->strategy->removeNeedlessBackups();

        $this->logger->info(sprintf('Список бэкапов в конце Backup@removeNeedlessBackups:%s%s', PHP_EOL, var_export($user->getBackups(), true)));

        return $user->getBackups();
    }

    /**
     * Создание бекапов БД и файлов
     *
     * @param User $user
     * @throws Exception
     */
    public function create(User $user): void
    {
        if (!$user->site_host) {
            return;
        }

        $this->logger->info(sprintf('Список бэкапов в начале Backup@create:%s%s', PHP_EOL, var_export($user->getBackups(), true)));

        $this->logger->info('БЕКАП для ' . $user->site_host);
        $doDbBackup = $this->strategy->checkBackupDb();
        $doFilesBackup = $this->strategy->checkBackupFiles();

        if (!$doDbBackup && !$doFilesBackup) {
            $this->logger->info('Пропустили бекап');
            return;
        }

        if (!is_dir($this->getPathUserDomain($user))) {
            $this->logger->info("Нет файлов домена {$this->getPathUserDomain($user)} на этом сервере");
            return;
        }

        if (!is_dir($this->getPathUserBackup($user)) && !mkdir($concurrentDirectory = $this->getPathUserBackup($user)) && !is_dir($concurrentDirectory)) {
            $this->logger->info("Создание {$this->getPathUserBackup($user)} завершилось неудачно");
            return;
        }


        if ($doDbBackup) {
            $backupDateTimeString = $this->dateUtils->getLocalDateTime();
            $isBackupSuccessful = $this->backupDatabase($user, $backupDateTimeString);
            if ($isBackupSuccessful) {
                $user->addBackup($backupDateTimeString);
                $this->strategy->removePrevBackupWithEqualHash();
            }
        }

        if ($doFilesBackup) {
            $this->backupImages($user);
        }

        $this->logger->info(sprintf('Список бэкапов в конце Backup@create:%s%s', PHP_EOL, var_export($user->getBackups(), true)));
    }

    /**
     * @param User $user
     * @return bool
     * @throws Exception
     */
    public function backupDatabase(User $user, string $dateTimeString): bool
    {
        $backupFileName = "{$this->getPathUserBackup($user)}/db-{$dateTimeString}.gz";

        if (file_exists($backupFileName)) {
            $this->logger->info("Бекап базы {$user->getDatabaseName()} за " . $dateTimeString . ' уже есть');
        } else {
            $this->logger->info("Старт бекапа для базы {$user->getDatabaseName()}");

            $login = $this->config->get('DB_LOGIN');
            $password = $this->config->get('DB_PASSWORD');
            $host = $this->config->get('DB_HOST');
            $port = $this->config->get('DB_PORT');
            $cliExecutor = new CliExecutor("mysqldump --skip-comments -u{$login} -p'{$password}' -h{$host} -P{$port} {$user->getDatabaseName()} | gzip -c > \"$backupFileName\"");
            $result = $cliExecutor->execute();
            if ($result) {
                $this->logger->info('Создание завершено успешно, размер ' . ceil(filesize($backupFileName) / 1024) . 'Кб');
                return filesize($backupFileName) > 0;
            }

            $this->logger->info($cliExecutor->getErrorMessage());
        }
        return false;
    }

    /**
     * @param string $sourcePath
     * @param string $targetPath
     * @param string $backupCommandPattern
     */
    private function doBackupFiles(string $sourcePath, string $targetPath, string $backupCommandPattern): void
    {
        $this->logger->info("Старт бекапа $sourcePath");

        if (is_dir($sourcePath)) {
            $files = glob($sourcePath . '/*');
            if (count($files) === 0) {
                $this->logger->info("Папка $sourcePath пуста");
            } else {
                $result = exec(sprintf($backupCommandPattern, $sourcePath, $targetPath), $result);
                $this->logger->info('Создание завершено успешно');
            }
        } else {
            $this->logger->info("Папка $sourcePath не существует");
        }
    }

    /**
     * @param User $user
     * @return void
     */
    private function backupImages(User $user): void
    {
        $skipFilePath = $this->getPathUserDomain($user) . '/.skip';
        if (file_exists($skipFilePath)) {
            $skipActions = file($skipFilePath, FILE_IGNORE_NEW_LINES);
            if (in_array('backup', $skipActions, true)) {
                $this->logger->info("Пропустил генерацию конфига для {$user->site_host}}");
                return;
            }
        }

        $this->doBackupFiles(
            $this->getPathUserDomain($user) . '/alboms',
            $this->getPathUserBackup($user) . '/alboms',
            'rdiff-backup --force --exclude-symbolic-links --exclude-sockets --exclude-special-files --exclude-fifos --exclude-device-files --no-hard-links --print-statistics --exclude "%1$s/*.css" --exclude "%1$s/*.js" --exclude "%1$s/*.cache" --exclude "%1$s/tmp/*" --exclude "%1$s/**/tmp" --exclude "%1$s/*/*/.*" %1$s %2$s'
        );
    }

    /**
     * @return string
     */
    public function getPathBackups(): string
    {
        return $this->path_backups;
    }

    /**
     * @param User $user
     * @return string
     */
    public function getPathUserBackup(User $user): string
    {
        return $this->getPathBackups() . '/' . $user->site_host;
    }

    /**
     * @param User $user
     * @return string
     */
    private function getPathUserDomain(User $user): string
    {
        return $this->getPathDomains() . '/' . $user->site_host;
    }

    /**
     * @return string
     */
    public function getPathDomains(): string
    {
        return $this->path_domains;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function getConfigValue(string $key)
    {
        return $this->config->get($key);
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * @return RateOptionInterface
     */
    public function getStrategy(): RateOptionInterface
    {
        return $this->strategy;
    }
}
