<?php

namespace ShopExpress\Backup;

class CliExecutor
{
    /**
     * @var string
     */
    private $command;

    /**
     * @var string
     */
    private $errorMessage;

    /**
     * @var bool
     */
    private $success;

    public function __construct(string $command)
    {
        $this->command = $command;
    }

    public function execute()
    {
        $descriptors = [
            0 => ['pipe', 'r'],
            1 => ['pipe', 'w'],
            2 => ['pipe', 'w']
        ];

        $procHandle = proc_open($this->command, $descriptors, $pipes);

        $stdErrOutput = '';
        $exitCode = 9999;

        if (is_resource($procHandle)) {
            fclose($pipes[0]);
            fclose($pipes[1]);

            if (!feof($pipes[2])) {
                $stdErrOutput .= stream_get_contents($pipes[2], -1, 0);
            }

            fclose($pipes[2]);

            $exitCode = proc_close($procHandle);
        }

        $output = array_filter(explode(PHP_EOL, $stdErrOutput), static function ($line) {
            $line = trim($line);
            if ($line == '') {
                return false;
            }

            if (mb_strpos($line, 'Using a password on the command line interface can be insecure') !== false) {
                return false;
            }

            return true;
        });

        if ($exitCode !== 0 || count($output)) {
            $this->errorMessage = implode(';' . PHP_EOL, $output);
            $this->success = false;
            return false;
        }

        $this->success = true;
        return true;
    }

    /**
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }
}
