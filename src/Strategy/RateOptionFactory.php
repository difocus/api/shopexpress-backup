<?php

namespace ShopExpress\Backup\Strategy;

use ShopExpress\Backup\Entity\User;
use ShopExpress\Backup\Strategy\Option\Rate14DaysOption;
use ShopExpress\Backup\Strategy\Option\RateBusinessOption;
use ShopExpress\Backup\Strategy\Option\RateEconomOption;
use ShopExpress\Backup\Strategy\Option\RateHostingOption;
use ShopExpress\Backup\Strategy\Option\RateLuxeOption;
use ShopExpress\Backup\Strategy\Option\RateMaximumOption;
use ShopExpress\Backup\Strategy\Option\RateStandartOption;
use ShopExpress\Backup\Strategy\Option\RateStubOption;
use ShopExpress\Backup\Strategy\Option\RateOptionInterface;

/**
 * Class RateOptionFactory
 * @package ShopExpress\Backup\Strategy
 */
class RateOptionFactory
{
    /**
     * @var array
     */
    protected $rateMapping;

    /**
     * RateOptionFactory constructor.
     */
    public function __construct()
    {
        $this->rateMapping = [
            Rate14DaysOption::getAlias() => Rate14DaysOption::class,
            RateStandartOption::getAlias() => RateStandartOption::class,
            RateBusinessOption::getAlias() => RateBusinessOption::class,
            RateEconomOption::getAlias() => RateEconomOption::class,
            RateMaximumOption::getAlias() => RateMaximumOption::class,
            RateLuxeOption::getAlias() => RateLuxeOption::class,
            RateStubOption::getAlias() => RateStubOption::class,
            RateHostingOption::getAlias() => RateHostingOption::class,
        ];
    }

    /**
     * @param User $user
     * @return RateOptionInterface
     */
    public function createByUser(User $user): RateOptionInterface
    {
        if (!array_key_exists($user->getRates(), $this->rateMapping)) {
            return new RateStubOption($user);
        }

        $className = $this->rateMapping[$user->getRates()];

        return new $className($user);
    }

    /**
     * @param string $alias
     * @param string $class
     * @return RateOptionFactory
     */
    public function addRateStrategy(string $alias, string $class): self
    {
        $this->rateMapping[$alias] = $class;
        return $this;
    }
}
