<?php

namespace ShopExpress\Backup\Strategy\Option;

use ShopExpress\Backup\Backup;
use ShopExpress\Backup\Entity\User;
use ShopExpress\Backup\Utils\DateUtils;
use ShopExpress\Backup\Utils\FileUtils;
use Throwable;

abstract class AbstractRateOption implements RateOptionInterface
{
    protected static $alias;

    protected $dbInterval;

    protected $filesInterval;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Backup
     */
    private $backupManager;

    /**
     * @return string
     */
    public static function getAlias()
    {
        return static::$alias;
    }

    /**
     * @param User $user
     * @return AbstractRateOption
     */
    public function setUser(User $user): RateOptionInterface
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param Backup $backupManager
     * @return $this
     */
    public function setBackupManager(Backup $backupManager): RateOptionInterface
    {
        $this->backupManager = $backupManager;
        return $this;
    }

    /**
     * SiteStrategy constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function checkPaid(): bool
    {
        return $this->backupManager->dateUtils->getTimestamp($this->user->getPaidtill()) > time();
    }

    public function checkBackupDb(): bool
    {
        $backups = $this->user->getBackups();
        $this->backupManager->getLogger()->info(sprintf('Последний бекап БД был %s', $backups[array_keys($backups)[count($backups) - 1]]));
        return true;
    }

    protected function getLastFilesBackup(): string
    {
        $stubLastFilesBackup = '1987-01-01 00:00:00';
        $filesBackups = [];
        $pathFilesBackups = "{$this->backupManager->getPathUserBackup($this->user)}/alboms";

        if (!is_dir($pathFilesBackups)) {
            return $stubLastFilesBackup;
        }

        exec("rdiff-backup --list-increments {$pathFilesBackups}", $filesBackups);
        $filesBackups = array_slice($filesBackups, 1);

        try {
            $filesBackups = array_map([$this->backupManager->dateUtils, 'parseRdiffBackupIncrementsLine'], $filesBackups);
            $lastFilesBackup = count($filesBackups) > 0 ? end($filesBackups) : $stubLastFilesBackup;
        } catch (Throwable $e) {
            $lastFilesBackup = $stubLastFilesBackup;
        }

        return $lastFilesBackup;
    }

    public function checkBackupFiles(): bool
    {
        $lastFilesBackup = $this->getLastFilesBackup();
        $tillFiles = $this->filesInterval - $this->backupManager->dateUtils->getDayIntervalSinceDateTime($lastFilesBackup);

        $this->backupManager->getLogger()->info(sprintf('Последний бекап файлов был %s, следущий через %s дней', $lastFilesBackup, $tillFiles));
        return $tillFiles <= 0;
    }

    public function checkPrevBackupWithSameHash(): bool
    {
        $backups = $this->user->getBackups();
        $pathUserBackup = $this->backupManager->getPathUserBackup($this->user);

        $countBackups = count($backups);

        if ($countBackups < 2) {
            return false;
        }

        $last_db_backup = $backups[array_keys($backups)[$countBackups - 1]];
        $prev_db_backup = $backups[array_keys($backups)[$countBackups - 2]];

        if ($this->backupManager->dateUtils->getTimestamp($prev_db_backup) < $this->backupManager->dateUtils->getFirstDayOfMonthTimestamp()) {
            return false;
        }

        $last_hash = FileUtils::getFileHash("{$pathUserBackup}/db-{$last_db_backup}.gz", [$this->backupManager->getLogger(), 'info']);
        $prev_hash = FileUtils::getFileHash("{$pathUserBackup}/db-{$prev_db_backup}.gz", [$this->backupManager->getLogger(), 'info']);

        if ($last_hash === -1 || $prev_hash === -1) {
            return false;
        }
        if (strcmp($last_hash, $prev_hash) === 0) {
            return true;
        }

        return false;
    }

    public function removePrevBackupWithEqualHash(): array
    {
        if (!$this->checkPrevBackupWithSameHash()) {
            return $this->user->getBackups();
        }

        $backups = $this->user->getBackups();

        $this->backupManager->getLogger()->info("New and previous db hashes are the same.");
        try {
            $keyRemoveBackup = array_keys($backups)[count($backups) - 2];
            FileUtils::removeBackupFromStorage($backups[$keyRemoveBackup], $this->backupManager->getPathUserBackup($this->user), [$this->backupManager->getLogger(), 'info']);
            $this->user->removeBackup($keyRemoveBackup);
        } catch (\Exception $e) {
        }
        return $this->user->getBackups();
    }

    /**
     * Удаляет лишние бекапы БД
     */
    public function removeNeedlessBackups(): void
    {
        if (!$this->user->site_host) {
            $this->backupManager->getLogger()->info("User Site Host is undefined");
        } else {
            $this->backupManager->getLogger()->info("User Site Host is " . $this->user->site_host);

            if (count($this->user->getBackups()) === 0) {
                $this->backupManager->getLogger()->info("Удалять нечего");
                return;
            }

            if ($this->backupManager->dateUtils->isLastDayOfMonth()) {
                $this->removeBackupsLastDayOfMonth();
            } else {
                $this->removeBackupsUsualDay();
            }
        }
    }

    /**
     * Удаляет бэкапы БД, если сегодня последний день месяца
     */
    public function removeBackupsLastDayOfMonth(): void
    {
        $this->backupManager->getLogger()->info("Today is last day of month");
        $index = null;
        $max = 0;
        $backups = $this->user->getBackups();

        foreach ($backups as $key => $value) {
            $this->backupManager->getLogger()->info("Backup: $value");
            if ($this->backupManager->dateUtils->getTimestamp($value) < $this->backupManager->dateUtils->getFirstDayOfMonthTimestamp()) {
                continue;
            }

            $fileSize = FileUtils::getBackupFileSize($value, $this->backupManager->getPathUserBackup($this->user), [$this->backupManager->getLogger(), 'info']);
            $this->backupManager->getLogger()->info("Filesize: $fileSize");
            if ($fileSize >= $max) {
                $max = $fileSize;
                $index = $key;
            }
        }

        $this->backupManager->getLogger()->info("Maximal filesize: $max");

        if ($max === FileUtils::NOT_FOUND) {
            $this->backupManager->getLogger()->info('Can\'t read file size of all elements');

            return;
        }

        if (!isset($index)) {
            $this->backupManager->getLogger()->info("There are no backups during last month");

            $index = array_keys($backups)[count($backups) - 1];
        }

        $this->backupManager->getLogger()->info("Removing backup except $index: {$backups[$index]}");

        FileUtils::removeBackupsExceptOne($backups, $index, $this->backupManager->getPathUserBackup($this->user), [$this->backupManager->getLogger(), 'info']);

        foreach ($backups as $key => $item) {
            if ($key !== $index) {
                $this->user->removeBackup($key);
            }
        }
    }

    private function removeBackupsUsualDay(): void
    {
        $backups = $this->user->getBackups();

        foreach ($backups as $key => $item) {
            if (!$this->backupManager->dateUtils->isLastWeek($item) &&
                $this->backupManager->dateUtils->getTimestamp($item) >= $this->backupManager->dateUtils->getFirstDayOfMonthTimestamp()) {

                $this->backupManager->getLogger()->info("Backup $item doesn't belongs to last week");
                try {
                    FileUtils::removeBackupFromStorage($item, $this->backupManager->getPathUserBackup($this->user), [$this->backupManager->getLogger(), 'info']);
                    $this->user->removeBackup($key);
                } catch (\Exception $e) {
                }
            }
        }
    }
}
