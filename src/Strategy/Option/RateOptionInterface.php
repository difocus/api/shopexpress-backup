<?php

namespace ShopExpress\Backup\Strategy\Option;

use ShopExpress\Backup\Backup;
use ShopExpress\Backup\Entity\User;

interface RateOptionInterface
{
    public function setBackupManager(Backup $backupManager): RateOptionInterface ;

    public function setUser(User $user): RateOptionInterface ;

    public function checkPaid(): bool;

    public function checkBackupDb(): bool;

    public function checkBackupFiles(): bool;

    public function removeNeedlessBackups();

    public function removePrevBackupWithEqualHash();
}
