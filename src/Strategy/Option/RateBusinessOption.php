<?php

namespace ShopExpress\Backup\Strategy\Option;

use ShopExpress\Backup\Entity\User;

class RateBusinessOption extends AbstractRateOption
{
    protected static $alias = 'business';

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->dbInterval = 0;
        $this->filesInterval = 0;
    }
}
