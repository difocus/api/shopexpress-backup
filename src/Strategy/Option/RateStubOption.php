<?php

namespace ShopExpress\Backup\Strategy\Option;

use ShopExpress\Backup\Backup;
use ShopExpress\Backup\Entity\User;

class RateStubOption implements RateOptionInterface
{
    protected static $alias = 'stub';
    /**
     * @var User
     */
    private $user;

    /**
     * @var Backup
     */
    private $backupManager;

    /**
     * @return string
     */
    public static function getAlias()
    {
        return static::$alias;
    }

    public function checkPaid(): bool
    {
        return true;
    }

    public function checkBackupDb(): bool
    {
        return true;
    }

    public function checkBackupFiles(): bool
    {
        return true;
    }

    public function setUser(User $user): RateOptionInterface
    {
        $this->user = $user;

        return $this;
    }

    public function setBackupManager(Backup $backupManager): RateOptionInterface
    {
        $this->backupManager = $backupManager;

        return $this;
    }

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function removeNeedlessBackups()
    {
    }

    public function removePrevBackupWithEqualHash()
    {
    }
}
