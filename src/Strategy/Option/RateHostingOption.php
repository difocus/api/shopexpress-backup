<?php

namespace ShopExpress\Backup\Strategy\Option;

use ShopExpress\Backup\Entity\User;

class RateHostingOption extends AbstractRateOption
{
    protected static $alias = 'hosting';

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->dbInterval = 0;
        $this->filesInterval = 0;
    }
}
