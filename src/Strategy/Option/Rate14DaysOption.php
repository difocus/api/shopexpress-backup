<?php

namespace ShopExpress\Backup\Strategy\Option;

use ShopExpress\Backup\Entity\User;

class Rate14DaysOption extends AbstractRateOption
{
    protected static $alias = '14days';

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->dbInterval = 0;
        $this->filesInterval = 0;
    }
}
