<?php

namespace ShopExpress\Backup\Strategy\Option;

use ShopExpress\Backup\Entity\User;

class RateEconomOption extends AbstractRateOption
{
    protected static $alias = 'economy';

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->dbInterval = 0;
        $this->filesInterval = 0;
    }
}
