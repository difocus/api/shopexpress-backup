<?php

namespace ShopExpress\Backup\Entity;

use DateTime;

class User
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var int
     */
    private $id;

    /**
     * @var array
     */
    private $initialBackups = [];

    /**
     * @var array
     */
    private $backups = [];

    /**
     * @var DateTime
     */
    private $paidtill;

    public function __construct(array $data = null)
    {
        $this->data = $data;
    }

    /**
     * @param $name
     * @return bool|mixed
     */
    public function __get($name)
    {
        $methodName = 'get' . ucfirst($name);
        if (method_exists($this, $methodName)) {
            return $this->$methodName();
        } elseif (isset($this->data[$name])) {
            return $this->data[$name];
        } else {
            return false;
        }
    }

    public function __isset($name)
    {
        $methodName = 'set' . ucfirst($name);
        return method_exists($this, $methodName) || isset($this->data[$name]);
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $methodName = 'set' . ucfirst($name);
        if (method_exists($this, $methodName)) {
            $this->$methodName($value);
        } else {
            $this->data[$name] = $value;
        }
    }

    /**
     * @param array $data
     * @return User
     */
    public function setData(array $data): User
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param int $userId
     * @return User
     */
    public function setId(int $userId): User
    {
        $this->id = $userId;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param $date
     * @return User
     */
    public function setPaidtill($date): User
    {
        if ($date instanceof DateTime) {
            $date = $date->format('Y-m-d');
        }
        $this->paidtill = $date;
        return $this;
    }

    /**
     * @return bool|mixed
     */
    public function getPaidtill()
    {
        return $this->paidtill;
    }

    /**
     * @param array $backups
     * @return User
     */
    public function setInitialBackups(array $backups): User
    {
        $this->initialBackups = $backups;
        return $this;
    }

    /**
     * @return array
     */
    public function getInitialBackups(): array
    {
        return $this->initialBackups;
    }

    /**
     * @return array|mixed|null
     */
    public function getBackups()
    {
        return $this->backups;
    }

    /**
     * @param array $backups
     * @return User
     */
    public function setBackups(array $backups): User
    {
        $this->backups = $backups;
        return $this;
    }

    /**
     * @param string $dateTimeBackup
     */
    public function addBackup(string $dateTimeBackup): void
    {
        $this->backups[] = $dateTimeBackup;
    }

    /**
     * @param int $key
     */
    public function removeBackup(int $key): void
    {
        if (array_key_exists($key, $this->backups)) {
            unset($this->backups[$key]);
        }
    }

    /**
     * @param string $rate
     * @return $this
     */
    public function setRates(string $rate): self
    {
        $this->data['rate'] = $rate;
        return $this;
    }

    public function getRates(): ?string
    {
        return $this->data['rate'];
    }

    public function getPrefix(): string
    {
        $this->data['prefix'] = $this->data['prefix'] ?? 'shopexpress';

        return $this->data['prefix'];
    }

    public function getDatabaseName(): string
    {
        return $this->prefix . '_' . $this->data['site_host'];
    }

    /**
     * @return User
     */
    public function sortBackups(): User
    {
        $backups = $this->getBackups();
        sort($backups, SORT_STRING);
        $this->setBackups($backups);

        return $this;
    }

    /**
     * @return array
     */
    public function getCreatedBackups(): array
    {
        $backups = [];
        foreach ($this->backups as $key => $value) {
            if (!array_key_exists($key, $this->initialBackups)) {
                $backups[$key] = $value;
            }
        }
        return $backups;
    }

    /**
     * @return array
     */
    public function getRemovedBackups(): array
    {
        $backups = [];
        foreach ($this->initialBackups as $key => $value) {
            if (!array_key_exists($key, $this->backups)) {
                $backups[$key] = $value;
            }
        }
        return $backups;
    }
}
