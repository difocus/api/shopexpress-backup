<?php


namespace ShopExpress\Backup;


use Exception;

class Config
{
    private $configs;

    /**
     * ConnectConfig constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->configs = $config;
    }

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function get($name)
    {
        if (!isset($this->configs[$name])) {
            throw new Exception("$name is not defined in Config");
        }
        return $this->configs[$name];
    }
}