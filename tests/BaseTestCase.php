<?php

namespace ShopExpress\Backup\Tests;

use Exception;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ShopExpress\Backup\Config;
use ShopExpress\Backup\Entity\User;
use ShopExpress\Backup\Strategy\RateOptionFactory;

abstract class BaseTestCase extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    protected static $root;

    /**
     * @var vfsStreamDirectory
     */
    protected static $backupsDir;

    /**
     * @var vfsStreamDirectory
     */
    protected static $domainsDir;

    /**
     * @var Config
     */
    protected static $config;

    /**
     * @var LoggerInterface
     */
    protected static $logger;

    /**
     * @var RateOptionFactory
     */
    protected static $strategyFactory;

    /**
     * @var DB
     */
    protected static $db;

    /**
     * @var string
     */
    protected static $siteDbName;

    /**
     * @throws Exception
     */
    public static function setUpBeforeClass(): void
    {
        date_default_timezone_set('Europe/Moscow');
        if (!file_exists(__DIR__ . '/.env.test')) {
            throw new Exception(sprintf('File .env.test does not exists in tests directory.'));
        }
        self::$config = new Config(parse_ini_file(__DIR__ . '/.env.test'));
        self::$logger = new NullLogger();
        self::$strategyFactory = self::createTestStrategyFactory();
        self::$db = new DB(self::$config);
    }

    /**
     * @throws Exception
     */
    public static function tearDownAfterClass(): void
    {
        self::$root = null;
        self::$backupsDir = null;
        self::$domainsDir = null;
        self::$config = null;
        self::$logger = null;
        if (self::$siteDbName) {
            self::$db->exec(sprintf('DROP DATABASE `%s`', self::$siteDbName));
        }
        self::$db = null;
    }

    protected static function createTestStrategyFactory(): RateOptionFactory
    {
        $strategyFactory = new RateOptionFactory();
        $strategyFactory->addRateStrategy(RateFakeStrategy::getAlias(), RateFakeStrategy::class);

        return $strategyFactory;
    }


    /**
     * @param $siteHost
     * @param $backupsContentMetadata
     * @throws Exception
     */
    protected function addDbBackups($siteHost, $backupsContentMetadata): void
    {
        $structure = [
            $siteHost => []
        ];

        foreach ($backupsContentMetadata as $dateTime => $localFileName) {
            $virtualFileName = sprintf('db-%s.gz', $dateTime);
            $localFilePath = __DIR__ . '/fixtures/' . $localFileName;

            $dumpContent = file_get_contents($localFilePath);
            if ($dumpContent === false) {
                throw new Exception(sprintf('Can\'t read local file %s', $localFilePath));
            }
            $structure[$siteHost][$virtualFileName] = $dumpContent;
        }

        vfsStream::create($structure, self::$backupsDir);
    }


    protected function getUserFromArray(array $metadata)
    {
        $user = new User();
        $user->setId($metadata['user_oid']);
        $user->setPaidtill($metadata['paid_till']['date']);
        $user->setData([
            'site_id' => $metadata['id'],
            'site_host' => $metadata['site_host'],
            'rate' => $metadata['tariff'],
        ]);
        $user->setInitialBackups($metadata['backups']);
        $user->setBackups($metadata['backups']);

        return $user;
    }

    protected function createSiteDb($dbName)
    {
        self::$siteDbName = $dbName;
        self::$db->exec(sprintf('CREATE DATABASE `%s` CHARACTER SET \'utf8\' COLLATE \'utf8_general_ci\'', $dbName));
    }

    protected function freshFilesystem(): void
    {
        self::$root = vfsStream::setup('root', null, [
            'var' => [
                'www' => [
                    'backups' => [],
                    'domains' => [],
                ],
            ]
        ]);

        self::$backupsDir = self::$root->getChild('var/www/backups');
        self::$domainsDir = self::$root->getChild('var/www/domains');
    }

    protected function removeFolderRecursive($folder): void
    {
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($folder, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($files as $fileInfo) {
            $command = ($fileInfo->isDir() ? 'rmdir' : 'unlink');
            $command($fileInfo->getRealPath());
        }
        rmdir($folder);
    }
}