#!/usr/bin/env bash
cd /tmp
rm -r ./backups/
rm -r ./domains/
mkdir ./backups
mkdir ./domains
cd ./domains; mkdir ./test; cd ./test; mkdir ./alboms; cd ./alboms; touch ./1; echo "Something amazing" > ./1
mysql -e "use shopexpress_backup_test;
DROP TABLE IF EXISTS objcontent; 
CREATE TABLE objcontent (ocid int(11) NOT NULL AUTO_INCREMENT,
  oid int(11) NOT NULL DEFAULT '0',
  name varchar(255) NOT NULL DEFAULT '',
  value text NOT NULL,
  created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (ocid),
  UNIQUE KEY uname (oid,name),
  KEY name (name),
  KEY value_index (value(255))); 
REPLACE INTO objcontent (oid, name, value) VALUES (99, 'site_host', 'test'), (99, 'email', 'foo@bar.com'), (99, 'paidtill', '2020-01-01 00:00:00'), (99, 'rate', 'continuous'); 
DROP DATABASE IF EXISTS shopexpress_test; 
CREATE DATABASE shopexpress_test;"