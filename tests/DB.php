<?php

namespace ShopExpress\Backup\Tests;

use PDO;
use PDOStatement;
use ShopExpress\Backup\Config;

/**
 * Class DB
 * @package DiCMS\Connection
 */
class DB extends PDO
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * DB constructor.
     *
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        parent::__construct(
            sprintf("mysql:dbname=%s;host=%s;port=%s",
                $this->config->get('DB_NAME'),
                $this->config->get('DB_HOST'),
                $this->config->get('DB_PORT')
            ),
            $this->config->get('DB_LOGIN'),
            $this->config->get('DB_PASSWORD'),
            [
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            ]
        );
        $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }
}
