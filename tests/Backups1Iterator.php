<?php

namespace ShopExpress\Backup\Tests;

use Iterator;

class Backups1Iterator implements Iterator
{
    /**
     * @var array
     */
    private $metadata;
    /**
     * @var int
     */
    private $position;
    /**
     * @var array
     */
    private $backupsContentMetadata;
    /**
     * @var array
     */
    private $results;
    /**
     * @var array
     */
    private $backupsDateTime;

    public function __construct(array $metadata, array $backupsContentMetadata, array $results, array $backupsDateTime)
    {
        $this->metadata = $metadata;
        $this->backupsContentMetadata = $backupsContentMetadata;
        $this->results = $results;
        $this->backupsDateTime = $backupsDateTime;
        
        $this->position = 0;
    }

    /**
     * @return array|mixed
     * @throws \Exception
     */
    public function current()
    {
        $this->metadata['backups'] = $this->backupsDateTime[$this->position];
        
        return [$this->results[$this->position], $this->metadata, $this->backupsContentMetadata[$this->position]];
    }
    
    public function next()
    {
        ++$this->position;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        return isset($this->results[$this->position]);
    }

    public function rewind()
    {
        $this->position = 0;
    }
}