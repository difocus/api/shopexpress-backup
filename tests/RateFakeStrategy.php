<?php

namespace ShopExpress\Backup\Tests;

use ShopExpress\Backup\Strategy\Option\AbstractRateOption;
use ShopExpress\Backup\Entity\User;

class RateFakeStrategy extends AbstractRateOption
{
    protected static $alias = 'fake';

    public function __construct(User $user)
    {
        parent::__construct($user);
        $this->dbInterval = 1;
        $this->filesInterval = 1;
    }
}