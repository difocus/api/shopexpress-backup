<?php


namespace ShopExpress\Backup\Tests\Unit;


use DateTime;
use DateTimeZone;
use Exception;
use ShopExpress\Backup\Backup;
use ShopExpress\Backup\Tests\Backups1Iterator;
use ShopExpress\Backup\Tests\BaseTestCase;
use ShopExpress\Backup\Utils\DateUtils;

class RemovePrevBackupWithEqualHashTest extends BaseTestCase
{
    public function backupsForRemovePrevBackupWithEqualHashProvider()
    {
        $userMetadata = [
            'site_host' => 'bar.com',
            'user_oid' => 1,
            'id' => 1,
            'paid_till' => [
                'date' => new DateTime('tomorrow', new DateTimeZone('Europe/Moscow'))
            ],
            'tariff' => 'fake',
            'backups' => []
        ];

        $backupsContentMetadata = [
            ['2019-01-01 00:00:00' => 'dump1.sql.gz'],
            [
                '2019-05-11 01:00:10' => 'dump1.sql.gz',
                '2019-05-12 01:00:10' => 'dump2.sql.gz',
            ],
            [
                '2019-05-11 01:00:10' => 'dump1.sql.gz',
                '2019-05-12 01:00:10' => 'dump1.sql.gz',
            ],
            [
                '2019-05-11 01:00:10' => 'nope.sql.gz',
                '2019-05-12 01:00:10' => 'nope.sql.gz',
            ]
        ];

        $backupsDateTime = [
            ['db-2019-01-01 00:00:00.gz'],
            ['db-2019-05-11 01:00:10.gz', 'db-2019-05-12 01:00:10.gz'],
            ['db-2019-05-11 01:00:10.gz', 'db-2019-05-12 01:00:10.gz'],
            ['db-2019-05-11 01:00:10', 'db-2019-05-12 01:00:10.gz'],
        ];

        $results = [
            [false, ['db-2019-01-01 00:00:00.gz']],
            [false, ['db-2019-05-11 01:00:10.gz', 'db-2019-05-12 01:00:10.gz']],
            [true, [1 => 'db-2019-05-12 01:00:10.gz']],
            [false, ['db-2019-05-11 01:00:10', 'db-2019-05-12 01:00:10.gz']],
        ];

        return new Backups1Iterator($userMetadata, $backupsContentMetadata, $results, $backupsDateTime);
    }

    /**
     * @dataProvider backupsForRemovePrevBackupWithEqualHashProvider
     * @param $result
     * @param array $metadata
     * @param array $backupsContentMetadata
     * @throws Exception
     */
    public function testItCanCheckPrevBackupHasSameHash($result, array $metadata, array $backupsContentMetadata): void
    {
        $this->freshFilesystem();
        $this->addDbBackups($metadata['site_host'], $backupsContentMetadata);

        $user = $this->getUserFromArray($metadata);

        $strategy = static::$strategyFactory->createByUser($user);
        $backup = new Backup(static::$config, static::$logger, $strategy, new DateUtils());


        $actualResult = $backup->getStrategy()->checkPrevBackupWithSameHash();
        self::assertSame($result[0], $actualResult);

        $backup->getStrategy()->removePrevBackupWithEqualHash();
        self::assertSame($result[1], $user->getBackups());
    }
}