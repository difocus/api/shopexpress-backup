<?php

namespace ShopExpress\Backup\Tests\Unit;

use DateTime;
use DateTimeZone;
use ShopExpress\Backup\Backup;
use ShopExpress\Backup\Config;
use ShopExpress\Backup\Strategy\Option\RateStubOption;
use ShopExpress\Backup\Tests\BaseTestCase;
use ShopExpress\Backup\Utils\DateUtils;

class BackupTest extends BaseTestCase
{
    private $createdFolder;

    public function databasesProvider(): array
    {
        return [
            [false, 'foo.com'],
            [true, 'bar.com'],
        ];
    }

    /**
     * @dataProvider databasesProvider
     * @throws \Exception
     */
    public function testItCanBackupDb($result, $dbName): void
    {
        $env = parse_ini_file(__DIR__ . '/../.env.test');
        $env['PATH_BACKUPS'] = __DIR__ . '/../backups';
        $this->createdFolder = $env['PATH_BACKUPS'];
        if (!is_dir($this->createdFolder)) {
            mkdir($this->createdFolder);
        }
        mkdir($this->createdFolder . '/' . $dbName);
        self::$config = new Config($env);

        $userMetadata = [
            'site_host' => $dbName,
            'userId' => 1,
            'paidtill' => new DateTime('tomorrow', new DateTimeZone('Europe/Moscow')),
            'email' => 'baz@kupikupi.org',
            'rates' => RateStubOption::getAlias(),
            'backups' => []
        ];

        $user = $this->getUserFromArray($userMetadata);
        $backupManager = new Backup(static::$config, static::$logger, new RateStubOption($user), new DateUtils());
        if ($result) {
            $this->createSiteDb($user->getDatabaseName());
        }

        $actualResult = $backupManager->backupDatabase($user, $backupManager->dateUtils->format(time()));
        self::assertSame($result, $actualResult);
    }

    public function tearDown(): void
    {
        $this->removeFolderRecursive($this->createdFolder);
        parent::tearDown();
    }
}
