<?php


namespace ShopExpress\Backup\Tests\Unit;


use DateTime;
use DateTimeZone;
use Exception;
use ShopExpress\Backup\Backup;
use ShopExpress\Backup\Tests\Backups1Iterator;
use ShopExpress\Backup\Tests\BaseTestCase;
use ShopExpress\Backup\Utils\DateUtils;

class RemoveNeedlessBackupsTest extends BaseTestCase
{
    /**
     * @return Backups1Iterator
     * @throws Exception
     */
    public function backupsForRemoveLastDayOfMonthProvider(): Backups1Iterator
    {
        $userMetadata = [
            'site_host' => 'bar.com',
            'user_oid' => 1,
            'id' => 1,
            'paid_till' => [
                'date' => new DateTime('tomorrow', new DateTimeZone('Europe/Moscow'))
            ],
            'tariff' => 'fake',
            'backups' => []
        ];

        $backupsContentMetadata = [
            [],
            ['2019-01-01 00:00:00' => 'dump1.sql.gz'],
            [
                '2019-04-30 23:59:59' => 'dump3.sql.gz',
                '2019-05-01 00:00:00' => 'dump1.sql.gz',
            ],
            [
                '2019-05-01 00:00:00' => 'dump3.sql.gz',
                '2019-05-02 00:00:00' => 'dump1.sql.gz',
            ],
            [
                '2019-05-03 00:00:00' => 'dump3.sql.gz',
                '2019-05-10 00:00:00' => 'dump3.sql.gz',
                '2019-05-11 00:00:00' => 'dump1.sql.gz',
            ],
        ];

        $backupsDateTime = [
            [],
            ['2019-01-01 00:00:00'],
            ['2019-04-30 23:59:59', '2019-05-01 00:00:00'],
            ['2019-05-01 00:00:00', '2019-05-02 00:00:00'],
            ['2019-05-03 00:00:00', '2019-05-10 00:00:00', '2019-05-11 00:00:00'],
        ];

        $results = [
            [],
            [0 => '2019-01-01 00:00:00'],
            [1 => '2019-05-01 00:00:00'],
            [0 => '2019-05-01 00:00:00'],
            [1 => '2019-05-10 00:00:00'],
        ];

        return new Backups1Iterator($userMetadata, $backupsContentMetadata, $results, $backupsDateTime);
    }

    /**
     * @dataProvider backupsForRemoveLastDayOfMonthProvider
     * @param $result
     * @param array $metadata
     * @param array $backupsContentMetadata
     * @throws Exception
     */
    public function testItCanRemoveBackupsLastDayOfMonth($result, array $metadata, array $backupsContentMetadata): void
    {
        $this->freshFilesystem();
        $this->addDbBackups($metadata['site_host'], $backupsContentMetadata);
        $user = $this->getUserFromArray($metadata);
        $strategy = static::$strategyFactory->createByUser($user);

        $dateUtils = $this->getMockBuilder(DateUtils::class)
            ->setMethods(['isLastDayOfMonth', 'getFirstDayOfMonthTimestamp'])
            ->getMock();

        $dateUtils->method('isLastDayOfMonth')
            ->willReturn(true);
        $dateUtils->method('getFirstDayOfMonthTimestamp')
            ->willReturn((new DateTime('2019-05-01 00:00:00'))->getTimestamp());
        /** @var DateUtils $dateUtils */

        $backup = new Backup(static::$config, static::$logger, $strategy, $dateUtils);

        $backups = $backup->removeNeedlessBackups($user);
        self::assertSame($result, $backups);
    }
}
