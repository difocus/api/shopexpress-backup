<?php

namespace ShopExpress\Backup\Tests\Unit;

use PHPUnit\Framework\TestCase;

use ShopExpress\Backup\Utils\DateUtils;

class DateUtilsTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        date_default_timezone_set('Europe/Moscow');
    }

    /**
     * @return array
     */
    public function rdiffBackupIncrementsListProvider(): array
    {
        return [
            ['Found 2 increments:                                                              ', false],
            ['    increments.2018-11-14T17:30:22+03:00.dir   Wed Nov 14 17:30:22 2018          ', '2018-11-14 17:30:22'],
            ["    increments.2018-11-14T17:30:22+03:00.dir\tWed Nov 14 17:30:22 2018          ", '2018-11-14 17:30:22'],
            ['Current mirror: Mon Dec 24 04:30:01 2018                                         ', '2018-12-24 04:30:01'],
            ['    increments.2020-04-01T18:34:39+03:00.dir   Wed Apr  1 18:34:39 2020', '2020-04-01 18:34:39'],
        ];
    }

    /**
     * @dataProvider rdiffBackupIncrementsListProvider
     */
    public function testItCanParseRdiffBackupIncrementsLines($line, $result)
    {
        $dateUtils = new DateUtils();
        self::assertTrue($dateUtils->parseRdiffBackupIncrementsLine($line) === $result);
    }

    public function testItCanGetIntervalSinceDateTime(): void
    {
        $dateUtils = $this->getMockWithGetCurrentTime(1558137600);
        // 05/18/2019 @ 12:00am (UTC)

        self::assertSame(1, $dateUtils->getDayIntervalSinceDateTime('2019-05-17 03:00:00'));
        self::assertSame(2, $dateUtils->getDayIntervalSinceDateTime('2019-05-17 02:59:59'));
        self::assertSame(0, $dateUtils->getDayIntervalSinceDateTime('2019-05-18 03:00:00'));
        self::assertSame(1, $dateUtils->getDayIntervalSinceDateTime('2019-05-17 23:59:59'));
        self::assertSame(-1, $dateUtils->getDayIntervalSinceDateTime('2019-05-19 23:59:59'));
    }

    public function testItCanGetTimestamp(): void
    {
        $dateUtils = new DateUtils();
        $timestamp = $dateUtils->getTimestamp('2019-05-17 23:59:59');
        // 05/17/2019 @ 8:59pm (UTC)
        self::assertSame(1558126799, $timestamp);
    }

    public function testItCanCheckIfTodayIsLastDayOfMonth(): void
    {
        $dateUtils = $this->getMockWithGetCurrentTime(1591056000);
        // 06/02/2020 @ 12:00am (UTC)

        self::assertTrue(!$dateUtils->isLastDayOfMonth());
    }



    public function testItCanGetRoundTimestampToday(): void
    {
        $dateUtils = new DateUtils();

        // 2019-05-17 00:00:00
        self::assertSame(1558051200, $dateUtils->getRoundTimestampToDay(1558109945));
    }

    public function testItCanGetFirstDayOfMonthTimestamp(): void
    {
        $dateUtils = new DateUtils();

        $dateTime = new \DateTime('2020-08-01 00:00:00');
        self::assertSame($dateTime->getTimestamp(), $dateUtils->getFirstDayOfMonthTimestamp());
    }

    public function testItCanCheckIfDateTimeIsInLastWeek(): void
    {
        // 2019-05-18 00:00:00
        $dateUtils = $this->getMockWithGetCurrentTime(1558137600);

        self::assertTrue($dateUtils->isLastWeek('2019-05-11 03:00:00'));
        self::assertTrue(!$dateUtils->isLastWeek('2019-05-11 02:59:59'));
    }


    public function testItCanGetFomat(): void
    {
        $dateUtils = new DateUtils();
        self::assertSame('2019-05-18 01:02:03', $dateUtils->format('18.05.2019 01:02:03'));
        self::assertSame('2019-05-18 01:02:03', $dateUtils->format(1558141323));
    }

    /**
     * @param int $timestamp
     * @return DateUtils
     */
    private function getMockWithGetCurrentTime(int $timestamp): DateUtils
    {
        $dateUtils = $this->getMockBuilder(DateUtils::class)
            ->setMethods(['getCurrentTime'])
            ->getMock();
        $dateUtils->method('getCurrentTime')
            ->willReturn($timestamp);
        /** @var DateUtils $dateUtils */
        return $dateUtils;
    }
}